<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bad_domains".
 *
 * @property int $id
 * @property string $name
 *
 * @property Click[] $clicks
 */
class BadDomains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bad_domains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @param $name
     * @param $col
     * @param $sort
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function searchByData($name, $col, $sort)
    {
        if ($sort == 'asc') {
            $sort = 4;
        } else {
            $sort = 3;
        }
        return self::find()->where(['like', $col, $name])->orderBy([$col => $sort])->asArray()->all();
    }

    public static function putNewRefer($data)
    {
        $searchParam = self::find()->where(['name' => $data['name']])->one();
        if ($searchParam) {
            return ["result" => false, 'name' => $searchParam->name, "id" => $searchParam->id];
        }
        $click_model = new self();
        $click_model->name = $data['name'];
        return ["result" => $click_model->save(), 'name' => $searchParam->name, "id" => $click_model->id];
    }

    public static function getStructOfTable()
    {
        return Yii::$app->db->createCommand("SHOW COLUMNS FROM bad_domains")->queryAll();
    }
}
