<?php

namespace app\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "click".
 *
 * @property int $id
 * @property string $ua
 * @property string $ip
 * @property string $ref
 * @property string $user_agent
 * @property int $param1
 * @property int $param2
 * @property string $error
 * @property int $bad_domain
 *
 * @property BadDomains $badDomain
 */
class Click extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'click';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['error', 'bad_domain'], 'integer'],
            [['id'], 'string', 'max' => 50],
            [['ua'], 'string', 'max' => 3],
            [['ip'], 'string', 'max' => 30],
            [['ref', 'param1', 'param2', 'user_agent'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ua' => 'Ua',
            'ip' => 'Ip',
            'ref' => 'Ref',
            'user_agent' => 'User Agent',
            'param1' => 'Param1',
            'param2' => 'Param2',
            'error' => 'Error',
            'bad_domain' => 'Bad Domain',
        ];
    }

    /**
     * @param $data
     * @return array
     */
    public static function putNewClick($data)
    {

        $searchParam = self::checkUnique($data);
        if ($searchParam['result']) {
            return ["result" => false, "id" => $searchParam['id']];
        }
        $click_model = new self();
        $click_model->id = uniqid();
        $click_model->ua = 'ua';
        $click_model->ip = Yii::$app->request->getUserIP();
        $click_model->ref = Yii::$app->request->referrer;
        $click_model->user_agent = Yii::$app->request->userAgent;
        $click_model->param1 = $data['param1'];
        $click_model->param2 = $data['param2'];
        if ($click_model->save()) {
            return ["result" => true, 'id' => $click_model->id];
        } else {
            Yii::$app->logs->saveLog(1, "Not writed in db!");
            return ["result" => false, "id" => $click_model->id];
        }
    }

    public static function getAll()
    {
        return self::find()->asArray()->all();
    }

    public static function searchByData($name, $col, $sort)
    {
        if ($sort == 'asc') {
            $sort = 4;
        } else {
            $sort = 3;
        }
        return self::find()->where(['like', $col, $name])->orderBy([$col => $sort])->asArray()->all();
    }

    public static function getStructOfTable()
    {
        return Yii::$app->db->createCommand("SHOW COLUMNS FROM click")->queryAll();
    }

    public static function checkUnique($data)
    {
        $dub_id = Click::find()
            ->where([
                'user_agent' => Yii::$app->request->userAgent,
                'ip' => Yii::$app->request->getUserIP(),
                'ref' => Yii::$app->request->referrer,
                'param1' => $data['param1'],
            ])
            ->one();

        if ($dub_id) {
            Yii::$app->logs->saveLog(1, "Duplicate data in db!");
            $dub_id->error = $dub_id->error + 1;
            $dub_id->save();
            return ['result' => true, 'id' => $dub_id['id']];
        }
        return ['result' => false, 'id' => $dub_id['id']];
    }
}
