<?php

use yii\db\Migration;

class m170530_144156_bad_domains extends Migration
{
    public function safeUp()
    {
        $this->createTable('bad_domains',[
            'id'=>$this->primaryKey(),
            'name'=>$this->string(255)
        ]);
        $this->createIndex(
            'name',
            'bad_domains',
            'name'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'name',
            'bad_domains'
        );
        $this->dropTable('bad_domains');
    }
}
