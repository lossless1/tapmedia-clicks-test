<?php

use yii\db\Migration;

class m170427_174506_logs extends Migration
{
    public function safeUp()
    {
        $this->createTable('logs', [
            'id_log' => $this->primaryKey(),
            'status' => $this->integer(),
            'text_log' => $this->text()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('logs');
    }
}
