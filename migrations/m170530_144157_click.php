<?php

use yii\db\Migration;

class m170530_144157_click extends Migration
{
    public function safeUp()
    {
        $this->createTable('click',[
            'id'=>$this->string(50),
            'ua' => $this->char(3),
            'ip' => $this->char(30),
            'ref'=> $this->string(255),
            'user_agent' => "text",
            'param1'=>$this->string(255),
            'param2'=>$this->string(255),
            'error'=>$this->integer(11)." DEFAULT 0",
            'bad_domain'=>"tinyint (1) DEFAULT 0"
        ]);
        $this->addPrimaryKey('id','click','id');
        $this->createIndex(
            'ref',
            'click',
            'ref'
        );
        $this->createIndex(
            'param1',
            'click',
            'param1'
        );
        $this->createIndex(
            'param2',
            'click',
            'param2'
        );
        $this->createIndex(
            'error',
            'click',
            'error'
        );
        $this->createIndex(
            'bad_domain',
            'click',
            'bad_domain'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'ref',
            'click'
        );
        $this->dropIndex(
            'param1',
            'click'
        );
        $this->dropIndex(
            'param2',
            'click'
        );
        $this->dropIndex(
            'error',
            'click'
        );
        $this->dropIndex(
            'bad_domain',
            'click'
        );
        $this->dropPrimaryKey('id','click');
        $this->dropTable('click');
    }
}
