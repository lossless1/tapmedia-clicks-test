<?php
/**
 * Created by PhpStorm.
 * User: lossless
 * Date: 4/27/17
 * Time: 20:38
 */

namespace app\components;

use Yii;
use yii\base\Component;
use app\models\Logs;
use yii\base\InvalidConfigException;

class LogComponent extends Component
{
    public function saveLog($status, $text)
    {
        $log = new Logs();
        $log->status = $status;
        $log->text_log = $text;
        $log->save();
    }
}