function Create() {

    this.sortOfTable = function () {
        var i, radioT, data;
        for (i = 0; i < 10; i++) {
            radioT = $("#radioT_" + i);
            if (radioT.prop("checked")) {
                radioTable = radioT.attr('data');
            }
        }
        $(".radio-column-sort").empty();
        $.get(radioTable + "/get-structure")
            .done(function (res) {
                data = JSON.parse(res);
                console.log(data);
                $.each(data, function (key, val) {
                    console.log(val.Field);
                    labelTag = $("<label>");
                    inputTag = $("<input>");
                    inputTag.attr('type','radio');
                    inputTag.attr('name','optradio');
                    inputTag.attr('id','radio_'+key);
                    inputTag.attr('data',val.Field);
                    spanTag = $("<span>");
                    spanTag.text(val.Field);
                    labelTag.append(inputTag);
                    labelTag.append(spanTag);
                    $(".radio-column-sort").append(labelTag);
                    $("#radio_0").prop("checked", "checked");
                });
            });
    };
    this.headOfTable = function (data) {
        var trTag, thTag;
        trTag = $("<tr>");
        $.each(data, function (key, value) {
            thTag = $("<th>");
            thTag.append(value.Field);
            trTag.append(thTag);
        });
        $("thead").append(trTag);
    };
    this.rowOfTable = function (data) {
        console.log(data);
        var trTag = $("<tr>");
        $.each(data, function (key, val) {
            var tdTag = $("<td>");
            tdTag.append(val);
            trTag.append(tdTag);
        });
        $("tbody").append(trTag);
    };
}