var nameDomain;

function Input() {
    this.nameToDomain = function(){
        nameDomain = $("#name-domain").val();
        console.log(nameDomain);

        if (nameDomain == '') {
            alert("Input more params");
            return;
        }
        $.get("bad/add-domain", {
            name: nameDomain
        }).done(function (val) {
            alert(val);
        })
    };

    this.paramsToClick = function(){
        var param1, param2;
        param1 = $("#param1").val();
        param2 = $("#param2").val();

        if (param1 == '' || param2 == '') {
            alert("Input more params");
            return;
        }
        $.get("click/index", {
            param1: param1,
            param2: param2
        }).done(function (val) {
            console.log(val);
        })
    };
    this.paramsByAutoClick = function(){
        $.get("click/add-click")
            .done(function (val) {
                console.log(val);
            })
    }
}