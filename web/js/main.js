var i, input, radioData, radio, radioV, radioSort, radioT, radioTable, data,create;

$(function () {
    input = new Input();

    $("#radio_0").prop("checked", "checked");
    $("#radioV_0").prop("checked", "checked");
    $("#radioT_0").prop("checked", "checked");

    $("#search-data").keypress(function (e) {
        if (e.which == 13) {
            search();
        }
    });
    $("#search-btn").click(function () {
        search();
    });

    $("#input-click-btn").click(function () {
        input.paramsToClick();
    });
    $("#input-domain-btn").click(function () {
        input.nameToDomain();
    });
    $("body").click(function () {
        var autoAdd = $("#auto-add-click").prop("checked");
        if (autoAdd) {
            input.paramsByAutoClick();
            console.log('Adding click to database');
        }
    });
    $("input[name='optTradio']").click(function () {
        create = new Create();
        create.sortOfTable();
    })
});


function search() {
    input = $("#search-data").val();
    for (i = 0; i < 10; i++) {
        radio = $("#radio_" + i);
        if (radio.prop("checked")) {
            radioData = radio.attr('data');
        }
        radioV = $("#radioV_" + i);
        if (radioV.prop("checked")) {
            radioSort = radioV.attr('data');
        }
        radioT = $("#radioT_" + i);
        if (radioT.prop("checked")) {
            radioTable = radioT.attr('data');
        }
        if (radioData != undefined && radioSort != undefined && radioTable != undefined) break;
    }
    $.post(radioTable + "/search-by-data", {
        'input': input,
        'select': radioData,
        'sort': radioSort
    }).done(function (res) {
        data = JSON.parse(res);
        showTable(data);
        console.log(data);
    });
}

function showTable(data) {
    create = new Create();

    $("thead").empty();
    $("tbody").empty();
    create.headOfTable(data.structure);

    $.each(data.data, function (key, val) {
        create.rowOfTable(val)
    });
}


