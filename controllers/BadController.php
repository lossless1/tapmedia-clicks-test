<?php

namespace app\controllers;

use app\models\BadDomains;
use app\models\Click;
use Yii;
use yii\db\Exception;
use yii\web\Controller;

/**
 * Class BadController
 * @package app\controllers
 */
class BadController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {

    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAddDomain()
    {
        $get = Yii::$app->request->get();
        if (!empty($get['name'])) {
            $save = BadDomains::putNewRefer($get);
            if ($save['result']) {
                return $this->redirect(['success/' . $save['id']]);
            } else {
                $dub_id = Click::find()->where([
                    'ref' => $save['name'],
                    'user_agent' => Yii::$app->request->userAgent,
                    'ip' => Yii::$app->request->getUserIP(),
                ])->one();
                if (!$dub_id) {
                    return json_encode('Domain not found in click table!');
                } else {
                    Yii::$app->db->createCommand()
                        ->update('click', ['bad_domain' => 1], 'ref = :ref')
                        ->bindValue("ref", $dub_id->ref)
                        ->execute();
                    Yii::$app->db->createCommand()
                        ->update('click', ['error' => $dub_id->error + 1], 'ref = :ref')
                        ->bindValue("ref", $dub_id->ref)
                        ->execute();
                    return $this->redirect(['error/' . $dub_id->id]);
                }
            }
        } else {
            return json_encode('Not enough arguments!');
        }
    }

    /**
     * @return string
     */
    public function actionSearchByData()
    {
        $post = Yii::$app->request->post();
        $data = BadDomains::searchByData($post['input'], $post['select'], $post['sort']);
        $structure = BadDomains::getStructOfTable();
        $result = [
            'structure' => $structure,
            'data' => $data
        ];
        return json_encode($result);
    }

    /**
     * @return bool|int
     */
    public function actionIsBadDomain()
    {
        $get = Yii::$app->request->get();
        $domain = Click::findOne([$get['id']]);
        if (!empty($domain) && $domain->bad_domain == 1) {
            return $domain->bad_domain;
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function actionGetStructure()
    {
        $data = BadDomains::getStructOfTable();
        return json_encode($data);
    }
}
