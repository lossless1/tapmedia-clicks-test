<?php

namespace app\controllers;

use yii\web\Controller;
use yii\helpers\Url;
use Yii;

/**
 * Class SuccessController
 * @package app\controllers
 */
class SuccessController extends Controller
{
    /**
     * @param $id
     * @return string
     */
    public function actionIndex($id)
    {
        return $this->render('index',['data'=>$id]);
    }
}