<?php

namespace app\controllers;

use app\models\Click;
use Yii;
use yii\db\Exception;
use yii\web\Controller;

/**
 * Class ClickController
 * @package app\controllers
 */
class ClickController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        $get = Yii::$app->request->get();
        if (!empty($get['param1']) && !empty($get['param2'])) {

            $save = Click::putNewClick($get);

            if ($save['result']) {
                return $this->redirect(['success/' . $save['id']]);
            } else {
                return $this->redirect(['error/' . $save['id']]);
            }
        }

        return $this->render('index', [
            'data' => 'Not enough arguments!'
        ]);
    }

    /**
     * @return string
     */
    public function actionSearchByData()
    {
        $post = Yii::$app->request->post();
        $structure = Click::getStructOfTable();
        $data = Click::searchByData($post['input'], $post['select'], $post['sort']);
        $result = [
            'structure' => $structure,
            'data' => $data
        ];
        return json_encode($result);
    }

    /**
     * @return string
     */
    public function actionAddClick()
    {
        $data['param1'] = md5(uniqid());
        $data['param2'] = md5(uniqid());
        $result = Click::putNewClick($data);
        return json_encode($result);
    }

    /**
     * @return string
     */
    public function actionGetStructure()
    {
        $data = Click::getStructOfTable();
        return json_encode($data);
    }
}
