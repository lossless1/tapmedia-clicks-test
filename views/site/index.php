<?php
$this->title = 'Main page';
?>
<div class="col-xs-12">
    <div class="input-group">
        <input class="form-control" type="text" id="search-data" placeholder="Search...">
        <div class="input-group-btn">
            <input class="btn btn-default" type="button" id="search-btn" value="GO">
        </div>
    </div>
</div>
<hr>
<div class="mix-group">
    <div class="col-xs-6 radio-tables">
        <label><input type="radio" name="optTradio" id="radioT_0" data="click"><span>Click table</span></label>
        <label><input type="radio" name="optTradio" id="radioT_1" data="bad"><span>Bad domains table</span></label>
    </div>
</div>
<hr>

<div class="mix-group">
    <div class="radio-buttons">
        <div>
            <p>Which column sort?</p>
        </div>
        <div class="radio-column-sort">
            <?php
            $i = 0;
            foreach ($struct as $key => $value) {
                echo "
                <label><input type='radio' name='optradio' id='radio_$i' data='{$value['Field']}'><span>{$value['Field']}</span></label>
            ";
                $i++;
            }
            ?>
        </div>
    </div>
    <div class="radio-sort">
        <div>
            <p>How sort?</p>
        </div>
        <div>
            <label><input type="radio" name="optVradio" id="radioV_0" data="asc"><span>ASC</span></label>
            <label><input type="radio" name="optVradio" id="radioV_1" data="desc"><span>DESC</span></label>
        </div>
    </div>
    <div class="col-xs-4 input-param">
        <label>Check to auto-adding data by clicking anything (warning!!)</label>
        <input class="custom-textbox" type="checkbox" id="auto-add-click">
    </div>
</div>
<hr>
<div class="row mix-group">
    <div class="col-xs-6">
        <div class="col-xs-4">
            <input class="form-control" type="text" id="param1" placeholder="param1...">
        </div>
        <div class="col-xs-4">
            <input class="form-control" type="text" id="param2" placeholder="param2...">
        </div>
        <div class="col-xs-3">
            <input class="btn btn-default" type="button" id="input-click-btn" value="Input click">
        </div>
    </div>
    <div class="col-xs-4">
        <div class="col-xs-6">
            <input class="form-control" type="text" id="name-domain" placeholder="Name...">
        </div>
        <div class="col-xs-4">
            <input class="btn btn-default" type="button" id="input-domain-btn" value="Input bad domain">
        </div>
    </div>
</div>
<hr>

<table class="table table-striped">
    <thead>
    <tr>
        <?php
        $i = 0;
        foreach ($struct as $key => $value) {
            echo "
            <th>{$value['Field']}</th>
        ";
            $i++;
        }
        ?>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $k => $value) {
            echo "<tr>";
            foreach ($value as $key => $val) {
                echo "
                    <td>{$value[$key]}</td>
                ";
            }
            echo "</tr>";
        }
        ?>
    </tbody>
</table>
